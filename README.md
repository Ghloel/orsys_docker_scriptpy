# bancaire_app_flask

# Getting started
#### Requirements :
- [ ] [Gitlab bancaire_app_flask](https://gitlab.com/Ghloel/bancaire_app_flask)
- [ ] [Python Latest](https://www.python.org/)
- [ ] [Flask](https://flask.palletsprojects.com/)

#### If you want use with docker :
- [ ] [Docker](https://www.docker.com/)


## Command
#### Windows :
```
git clone https://gitlab.com/Ghloel/bancaire_app_flask.git
cd bancaire_app_flask/
pyton -m venv venv/
venv/Script/activate
pip install -r requirements.txt
python main.py
```

#### Docker :
```
git clone -b docker https://gitlab.com/Ghloel/bancaire_app_flask.git
cd bancaire_app_flask
sudo docker build -t orsys_script .
sudo docker run -d -p 5000:5000 orsys_script
```
