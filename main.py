import datetime
import os
import sys

from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

file_proprietaire = os.getcwd() + '/proprietaire.txt'
file_compte = os.getcwd() + '/compte.txt'

data = {}


def init_prog():
    data = {}
    get_data_proprietaire()
    get_data_compte()


def get_data_proprietaire():
    if os.path.exists(file_proprietaire):
        csv_data = []
        proprietaire = open(file_proprietaire)
        for line in proprietaire:
            data_line = line.rstrip().split('\t')
            for element in data_line:
                csv_data.append(element)
        if csv_data:
            del csv_data[0]

            for element in csv_data:
                element = element.split(';')
                data[element[0]] = {
                    "info": {
                        "name": element[1],
                        "cin": element[2],
                        "phone": element[3],
                        "address": element[4],
                    },
                    "compte": []
                }


def save():
    result_compte = 'idCompte # idProprietaire # solde # jj - mm - aaaa # type\n'
    for proprietaire in data:
        # nb_compte = len(data[proprietaire]["compte"])
        for compte in data[proprietaire]["compte"]:
            tmp_data = []
            count_data = 0
            for element in compte:
                if count_data == 1:
                    tmp_data.append(proprietaire)
                    tmp_data.append(compte[element])
                else:
                    tmp_data.append(compte[element])
                count_data += 1
            result_compte += '#'.join(tmp_data)
            result_compte += '\n'
    print(result_compte[:-1], file=sys.stderr)
    f = open(file_compte, 'w+')
    f.write(result_compte)

    result_proprietaire = 'idProprietaire;nom;CIN;telephone;adresse\n'
    nb_proprietaire = len(data)
    nb_count = 1
    for proprietaire in data:
        id = str(proprietaire)
        name = str(data[proprietaire]["info"]["name"])
        cin = str(data[proprietaire]["info"]["cin"])
        phone = str(data[proprietaire]["info"]["phone"])
        address = str(data[proprietaire]["info"]["address"])
        result_proprietaire += id + ";" + name + ";" + cin + ";" + phone + ";" + address
        if nb_count != nb_proprietaire:
            result_proprietaire += "\n"
        nb_count += 1
    f = open(file_proprietaire, 'w+')
    f.write(result_proprietaire)


def get_data_compte():
    if os.path.exists(file_compte):
        csv_data = []
        proprietaire = open(file_compte)
        for line in proprietaire:
            data_line = line.rstrip().split('\t')
            for element in data_line:
                csv_data.append(element)
        if csv_data:
            del csv_data[0]
            # print(csv_data)
            for element in csv_data:
                element = element.split('#')
                data[element[1]]['compte'].append({
                    "idc": element[0],
                    "solde": element[2],
                    "date": element[3],
                    "type": element[4]
                })


def search_account(id):
    result = None
    for proprietaire in data:
        for compte in data[proprietaire]["compte"]:
            for idc in compte['idc']:
                if int(idc) == int(id):
                    print(idc)
                    result = data[proprietaire]

    if result:
        print(result)
    else:
        print('Non trouver')


def get_last_id_account():
    last_account = None
    for proprietaire in data:
        for compte in data[proprietaire]["compte"]:
            for idc in compte['idc']:
                if last_account is None:
                    last_account = idc
                else:
                    if int(last_account) < int(idc):
                        last_account = idc
    return last_account


def get_last_id_proprietaire():
    last_account = None
    for proprietaire in data:
        if last_account is None:
            last_account = proprietaire
        else:
            if int(last_account) < int(proprietaire):
                last_account = proprietaire
    return last_account


def add_compte(name, cin, phone, adress, solde, type):
    last_id_proprietaire = int(get_last_id_proprietaire()) + 1
    last_id_account = int(get_last_id_account()) + 1
    data[str(last_id_proprietaire)] = {
        "info": {
            "name": name,
            "cin": cin,
            "phone": phone,
            "address": adress,
        },
        "compte": [{
            'idc': str(last_id_account),
            'solde': str(solde),
            'date': datetime.datetime.today().strftime('%d-%m-%Y'),
            'type': type
        }]
    }
    save()
    init_prog()


def del_account(id_account):
    init_prog()
    for proprietaire in data:
        for compte in data[proprietaire]["compte"]:
            index_account = 0
            for idc in compte['idc']:
                print(idc)
                print(id_account)
                if int(idc) == int(id_account):
                    print(data, file=sys.stderr)
                    del data[proprietaire]['compte'][index_account]
                    # print(data, file=sys.stderr)
                    save()
                    init_prog()
                index_account += 1


# if __name__ == '__main__':
#     init_prog()
#     # search_account(1)
#     print("Denier compte id ", get_last_id_account())
#     add_compte("test4", "qsdf45dsqf56", "0245694521", "rue de test4", "9999", "true")
#     del_account(4)
#     save()

@app.route('/', methods=['GET', 'POST'])
def index():
    init_prog()
    # # search_account(1)

    result = {
        'all_account': data,
        'last_account': get_last_id_account()
    }
    # print(result, file=sys.stderr)
    if request.method == 'POST':
        print(request.form, file=sys.stderr)
        if 'submit_add' in request.form:
            print('submit_add', file=sys.stderr)
            add_compte(
                request.form['add_nom'],
                request.form['add_cin'],
                request.form['add_telephone'],
                request.form['add_address'],
                request.form['add_solde'],
                request.form['add_type']
            )
            # add_compte("test4", "qsdf45dsqf56", "0245694521", "rue de test4", "9999", "true")

            return redirect(url_for('index'))
        if 'submit_del' in request.form:
            print('submit_del', file=sys.stderr)
            del_account(request.form['del_id'])
            return redirect(url_for('index'))

    return render_template('index.html', name="Accueil", result=result)


if __name__ == "__main__":
    app.run()
